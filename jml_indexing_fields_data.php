<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

// Aim: Collect statistics on mysql unified search index

// Perform basic checks
info("Verifying...");

//echo "JML: argc: " . $_SERVER['argc'] . "\n";

if (! isset($_SERVER['argc']) || $_SERVER['argc'] != 3) {
	error("Missing argument. \n\nSyntax: " . __FILE__ . " path/to/local.php index\n");
}

$dblocal = $_SERVER['argv'][1];
$index = $_SERVER['argv'][2];

include($dblocal);

important("database: $dbs_tiki");
important("index: $index");

$con = mysqli_connect($host_tiki,$user_tiki,$pass_tiki,$dbs_tiki);
if (mysqli_connect_errno()) {
    printf("Connection failure: %s\n", mysqli_connect_error());
    exit();
}
$table = $index;

/**
* Get the column names for a mysql table
**/

$sql = 'DESCRIBE '.$table;
if ($result = mysqli_query($con, $sql)) {
	important("Number of index fields: " . mysqli_num_rows($result));

	$length = 0;
	$number = 0;
	// Calculations of rely on https://dev.mysql.com/doc/refman/5.7/en/create-table-files.html#limits-frm-file
	$calculated_value = 288;
	while($row = mysqli_fetch_assoc($result)) {
		$calculated_value += strlen($row['Field']) + 1 + 17;
		$number++;
		$length += strlen($row['Field']);
	}
	mysqli_free_result($result);
}

important("Number of fields: " .$number);
important("Length of fields: " .$length);
important("Calculate value: " .$calculated_value);

for ($i=1; $i < 1000 ; $i++) {
	#$fieldname = sprintf("azertyuiop0%09d", $i);
	$fieldname = sprintf("%07d", $i);
	#$fieldname = sprintf("%d", $i);
	#$fieldname = sprintf("%05d", $i);
	info($fieldname);
	$calculated_value += strlen($fieldname) + 18;
	$sql = "ALTER TABLE `$table` ADD COLUMN `" . $fieldname . "` TEXT;";
	if (mysqli_query($con, $sql) === TRUE) {
		important("OK: $calculated_value");
	} else {
		error("Failed adding field $fieldname ($calculated_value) " . mysqli_error($con) );
	}
}

die('JML testing');

/**
 * @param $message
 */
function error($message)
{
        die(color($message, 'red') . "\n");
}

/**
 * @param $message
 */
function info($message)
{
        echo $message . "\n";
}

/**
 * @param $message
 */
function important($message)
{
        echo color($message, 'green') . "\n";
}

/**
 * @param $string string            String to output
 * @param $color string                The colour of the string
 * @return string                    The formatted string to output to the console
 */
function color($string, $color)
{
        $avail = [
                'red' => 31,
                'green' => 32,
                'yellow' => 33,
                'blue' => 34,
                'purple' => 35,
                'cyan' => 36,
                'gray' => 37,
        ];

        if (! isset($avail[$color])) {
                return $string;
        }

        return "\033[{$avail[$color]}m$string\033[0m";
}
